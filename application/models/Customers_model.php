<?php

class Customers_model extends CI_Model
{
    /**
     * Customers_model constructor.
     */
    public function __construct()
    {
        $this->load->database();
    }

    /**
     * @return array
     */
    public function getCustomers(): array
    {
        /** @var CI_DB_result $query */
        $query = $this->db->get('customers');

        return $query->result_array();
    }
}