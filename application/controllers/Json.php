<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  customers_model
 */
class Json extends CI_Controller
{
    /**
     * Json constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('customers_model');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        header('Content-Type: application/json');
        /** @var Customers_model $controllerModel */
        $controllerModel = $this->customers_model;

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($controllerModel->getCustomers()));
    }
}