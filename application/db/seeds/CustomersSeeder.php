<?php


use Phinx\Seed\AbstractSeed;

class CustomersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'first_name'        => 'Ignacio',
                'last_name'         => 'Salcedo',
                'email'             => 'ignacio@isalcedo.com',
                'business_name'     => 'isalcedo.com',
                'registration_date' => date('Y-m-d H:i:s')
            ],
            [
                'first_name'        => 'Sophia',
                'last_name'         => 'Salcedo',
                'email'             => 'sophia@isalcedo.com',
                'business_name'     => 'Sophia Designs',
                'registration_date' => date('Y-m-d H:i:s')
            ]
        ];

        $customers = $this->table('customers');
        $customers->insert($data)
            ->save();
    }
}
